---- Reload configurations if we modify plugins.lua
---- Hint
----     <afile> - replaced with the filename of the buffer being manipulated
--vim.cmd([[
--  augroup packer_user_config
--    autocmd!
--    autocmd BufWritePost plugins.lua source <afile> | PackerSync
--  augroup end
--]])

-- Bootstrap Lazy
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
	vim.fn.system({
		"git",
		"clone",
		"--filter=blob:none",
		"https://github.com/folke/lazy.nvim.git",
		"--branch=stable", -- latest stable release
		lazypath,
	})
end
vim.opt.rtp:prepend(lazypath)

return require("lazy").setup({
	{ "ellisonleao/gruvbox.nvim", priority = 1000 },
	--{
	--	"Vigemus/iron.nvim",
	--	config = function()
	--		require("config.iron")
	--	end,
	--},
	--{
	--	"dccsillag/magma-nvim",
	--	config = true,
	--},
	{ "nvim-treesitter/nvim-treesitter", build = ":TSUpdate" },
	{
		"geg2102/nvim-python-repl",
		dependencies = "nvim-treesitter",
		ft = { "python", "lua" },
		config = function()
			require("config.repl")
		end,
	},
	{
		"stevearc/conform.nvim",
		config = function()
			require("config.conform")
		end,
	},
	{
		"hrsh7th/nvim-cmp",
		dependencies = {
			"hrsh7th/cmp-nvim-lsp",
			"hrsh7th/cmp-buffer",
		},
		config = function()
			require("config.nvim-cmp")
		end,
	},
	{ "hrsh7th/cmp-nvim-lsp", dependencies = { "nvim-cmp" } },
	{ "hrsh7th/cmp-buffer", dependencies = { "nvim-cmp" } }, -- buffer auto-completion
	{ "hrsh7th/cmp-path", dependencies = { "nvim-cmp" } }, -- path auto-completion
	{ "hrsh7th/cmp-cmdline", dependencies = { "nvim-cmp" } }, -- cmdline auto-completion
	{
		"L3MON4D3/LuaSnip",
		build = "make install_jsregexp",
	},
	"saadparwaiz1/cmp_luasnip",
	{ "lukas-reineke/cmp-under-comparator", dependencies = { "nvim-cmp" } },
	-- Deprioritizes underline symbols

	"williamboman/mason.nvim",
	"williamboman/mason-lspconfig.nvim",
	"neovim/nvim-lspconfig",
	{
		"WhoIsSethDaniel/mason-tool-installer.nvim",
		config = function()
			require("config.mason-tool-installer")
		end,
	},
	{
		"nvim-telescope/telescope.nvim",
		dependencies = { "nvim-lua/plenary.nvim" },
	},
	{
		"folke/neodev.nvim",
		config = function()
			require("neodev").setup({
				library = { plugins = { "nvim-dap-ui" }, types = true },
			})
		end,
	},
	{
		"folke/trouble.nvim",
		dependencies = { "nvim-tree/nvim-web-devicons" },
		opts = {},
	},
	{
		"mfussenegger/nvim-lint",
		config = function()
			require("config.lint")
		end,
	},
	{
		"rcarriga/nvim-dap-ui",
		dependencies = { "mfussenegger/nvim-dap", "folke/neodev.nvim" },
		config = function()
			require("dapui").setup()
		end,
	},
	{
		"mfussenegger/nvim-dap-python",
		dependencies = { "mfussenegger/nvim-dap" },
		config = function()
			require("dap-python").setup("/usr/bin/python")
			require("dap").defaults.fallback.exception_breakpoints = { "raised" }
		end,
	},
	--use { 'taDachs/ros-nvim',
	--      requires = {
	--        {'nvim-telescope/telescope.nvim'},
	--        { 'nvim-treesitter/nvim-treesitter', run = ':TSUpdate' }
	--      },
	--      config = function() require("ros-nvim").setup({only_workspace = true, ':TSInstall ros'}) end,
	--}

	-- Automatically set up your configuration after cloning packer.nvim
	-- Put this at the end after all plugins
	--if packer_bootstrap then
	--    require('packer').sync()
	--end
})
