require("conform").setup({
	formatters_by_ft = {
		lua = { "stylua" },
		python = { "isort", "black", "docformatter" },
	},
})

local util = require("conform.util")
local black = require("conform.formatters.black")
require("conform").formatters.black = vim.tbl_deep_extend("force", black, {
	args = util.extend_args(black.args, { "--line-length", "79", "--preview" }), -- use preview for string formatting which will become default in 2024
})

vim.api.nvim_create_autocmd("BufWritePre", {
	pattern = "*",
	callback = function(args)
		require("conform").format({ bufnr = args.buf })
	end,
})
