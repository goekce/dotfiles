require("lint").linters_by_ft = {
	python = { "pylint" },
}

require("lint").linters.pylint.args = {
	"-f",
	"json",
	"--init-hook",
	"import sys; sys.path.append('/usr/lib/python3.11/site-packages')",
	"--disable",
	"C0114,C0115,C0116",
}
vim.api.nvim_create_autocmd("BufWritePost", {
	callback = function()
		require("lint").try_lint()
	end,
})
