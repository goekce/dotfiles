require("nvim-python-repl").setup({
	execute_on_send = true,
	vsplit = true,
})

vim.api.nvim_command("tnoremap <Esc> <C-><C-n>")
vim.api.nvim_command("autocmd BufEnter term://* startinsert")
vim.api.nvim_command("noremap <C-h> <C-><C-N><C-w>h")
vim.api.nvim_command("noremap <C-j> <C-><C-N><C-w>j")
vim.api.nvim_command("noremap <C-k> <C-><C-N><C-w>k")
vim.api.nvim_command("noremap <C-l> <C-><C-N><C-w>l")
vim.api.nvim_command("autocmd TermOpen * setlocal conceallevel=0 colorcolumn=0 relativenumber")
