-- define common options
local opts = {
	noremap = true, -- non-recursive
	silent = true, -- do not show message
}

-----------------
-- Normal mode --
-----------------

-- Hint: see `:h vim.map.set()`
-- Better window navigation
vim.keymap.set("n", "<C-h>", "<C-w>h", opts)
vim.keymap.set("n", "<C-j>", "<C-w>j", opts)
vim.keymap.set("n", "<C-k>", "<C-w>k", opts)
vim.keymap.set("n", "<C-l>", "<C-w>l", opts)

-- Resize with arrows
-- delta: 2 lines
vim.keymap.set("n", "<C-Up>", ":resize -2<CR>", opts)
vim.keymap.set("n", "<C-Down>", ":resize +2<CR>", opts)
vim.keymap.set("n", "<C-Left>", ":vertical resize -2<CR>", opts)
vim.keymap.set("n", "<C-Right>", ":vertical resize +2<CR>", opts)

-- Save
vim.keymap.set("n", "<leader>z", ":update<CR>", opts)

-- Telescope
local builtin = require("telescope.builtin")
vim.keymap.set("n", "<leader>ff", builtin.find_files, {})
vim.keymap.set("n", "<leader>fg", builtin.live_grep, {})
vim.keymap.set("n", "<leader>fb", builtin.buffers, {})
vim.keymap.set("n", "<leader>fh", builtin.help_tags, {})

-- Trouble
vim.keymap.set("n", "<leader>xx", function()
	require("trouble").toggle()
end)
vim.keymap.set("n", "<leader>xw", function()
	require("trouble").open("workspace_diagnostics")
end)
vim.keymap.set("n", "<leader>xd", function()
	require("trouble").open("document_diagnostics")
end)
vim.keymap.set("n", "<leader>xq", function()
	require("trouble").open("quickfix")
end)
vim.keymap.set("n", "<leader>xl", function()
	require("trouble").open("loclist")
end)
vim.keymap.set("n", "gR", function()
	require("trouble").open("lsp_references")
end)

-- dap
vim.keymap.set("n", "<leader>b", function()
	require("dap").toggle_breakpoint()
end, opts)
vim.keymap.set("n", "<leader>c", function()
	require("dap").continue()
end, opts)
vim.keymap.set("n", "<leader>s", function()
	require("dap").step_over()
end, opts)
vim.keymap.set("n", "<leader>i", function()
	require("dap").step_into()
end, opts)
vim.keymap.set("n", "<leader>o", function()
	require("dap").step_out()
end, opts)
vim.keymap.set("n", "<leader>t", function()
	require("dap").terminate()
end, opts)

-- dapui
-- lua require('dapui').open()
vim.keymap.set("n", "<leader>d", function()
	require("dapui").toggle()
end, opts)

-- dap-python
vim.keymap.set("n", "<leader>dn", function()
	require("dap-python").test_method()
end, opts)
vim.keymap.set("n", "<leader>df", function()
	require("dap-python").test_class()
end, opts)
vim.keymap.set("v", "<leader>ds <ESC>", function()
	require("dap-python").debug_selection()
end, opts)

-- ros-nvim
-- telescope finder
vim.keymap.set("n", "<leader>tr", "<cmd>Telescope ros ros<cr>", { noremap = true })

-- follow links in launch files
vim.keymap.set("n", "<leader>rol", function()
	require("ros-nvim.ros").open_launch_include()
end, opts)

-- show definition for interfaces (messages/services) in floating window
vim.keymap.set("n", "<leader>rdi", function()
	require("ros-nvim.ros").show_interface_definition()
end, opts)

-- access config
vim.keymap.set("n", "<leader>vce", "<cmd>tabnew ~/.config/nvim/lua<cr>", opts)
vim.keymap.set("n", "<leader>vcr", "<cmd>so $MYVIMRC<cr>", opts)

-- nvim-python-repl
vim.keymap.set("n", "<space>rs", function()
	require("nvim-python-repl").send_statement_definition()
end, { desc = "Send semantic unit to REPL" })
vim.keymap.set("v", "<space>rs", function()
	require("nvim-python-repl").send_visual_to_repl()
end, { desc = "Send visual selection to REPL" })
vim.keymap.set("n", "<space>rr", function()
	require("nvim-python-repl").send_buffer_to_repl()
end, { desc = "Send entire buffer to REPL" })
vim.keymap.set("n", "<space>ra", function()
	require("nvim-python-repl").toggle_execute()
end, { desc = "Automatically execute command in REPL after sent" })
vim.keymap.set("n", "<space>rt", function()
	require("nvim-python-repl").toggle_vertical()
end, { desc = "Create REPL in vertical or horizontal split" })

-- Terminal mode
vim.keymap.set("t", "<Esc>", "<C-><C-n>", opts)

-----------------
-- Visual mode --
-----------------

-- Hint: start visual mode with the same area as the previous area and the same mode
vim.keymap.set("v", "<", "<gv", opts)
vim.keymap.set("v", ">", ">gv", opts)
